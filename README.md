# MoTH - Monitoring of Temperature and Humidity

MoTH is an Arduino (Mega) shield for environmental monitoring up to 8 experimental setups.



## Sensors
The shield is designed to use analog sensors only.

The humidity sensor the board is designed for is a calibrated Honeywell sensor of series [HIH4000-003/4](https://sensing.honeywell.com/honeywell-sensing-hih4000-series-product-sheet-009017-5-en.pdf), or preferably [HIH4010/4020/4021-003/4](https://sps.honeywell.com/us/en/products/sensing-and-iot/sensors/humidity-with-temperature-sensors/hih-4010-4020-4021-series) with a protected housing. They require an 80kOhm load resistor between Vout and GND.

The NTC or any thermistor is connected between GND (right pin) and Vout (left pin), where the series resistor for a voltage divider is connected between Vout and the supply voltage (5V).


## PCB

The PCB provides positions of 8 environmental sensors (4 pin) and 16 NTCs (3 pin) - 8 for devices under test (DUT), e.g. pixel modules, and 8 redundant ones that can be used e.g. for cooling. The environmental sensors occupy all analog pins of an Arduino Mega with 10 bit ADCs. The 16 NTCs are connected to an IO-expander, [MCP23017](https://ww1.microchip.com/downloads/en/DeviceDoc/20001952C.pdf) which is implemented in [labRemote](https://gitlab.cern.ch/berkeleylab/labRemote/-/blob/main/src/libDevCom/MCP23017.cpp), communicates through up to 1.7 MHz I2C interface, and has 16 bit ADCs.

The 2.54 mm pitch slots can use simple pinheaders, or Molex [Mini-Latch](https://www.molex.com/molex/products/family/minilatch) wire-to-board connectors.

### 4-pin connector

#### Environmental slots

The 4-pin slots labeled with "ENV" can be used to connect only an NTC or only a humidity sensor by using pins 3+4 or 1-3, respectively, as shown below.

R_T indicates the series resistor for the voltage divider required for the NTC.
R_H is the load resistor for the humidity sensor.

The labeling ETx and EHx in the environmental sensor indicates the pin for temperature (T) and humidity (H), respectively, where x is the analog pin of the Arduino Mega the signal is connected to.

![NTC only](docs/env_ntc.png) ![Hum only](docs/env_hum.png)

#### LCD slot

The vertical 4-pin slot in the upper right corner can be used to add an LCD screen using the Adafruit [LCD backpack](https://learn.adafruit.com/i2c-spi-lcd-backpack).

### 3-pin connector

The 3-pin slots can be used to connect NTCs in 2 ways
1. Use pin 1+2 and solder the series resistor onto the PCB.
2. Use all pins with the series resistor connected already on the socket. In this case leave the SMD pads empty.

NTCs 1-8 use GPA0-GPA7 on the IO-expander and NTCs 9-18 use GPB0-GPB7 on the IO-expander.

![2 pin](docs/ntc_2pin.png) ![3 pin](docs/ntc_3pin.png)

## Sensor Adapter
The sensor adapter is designed to facilitate soldering, moving away from soldering 3 pins and cables together. On the sensor side, it provides the footprint for both HIH4000-003 with 2.54 mm and HIH4000-004 with 1.27 mm pitch, and on the backside is the footprint for the Semitec NTC with 1.8 mm pitch. On the connector side, one can use a simple pinheader with 1.27 mm pitch as used in the SHT85. This way our environmental sensor obtains a similar form factor as the SHT85 with identical connector pitch.

![sensor adapter](docs/sensoradapter.png)
adapter PCB with sensors

![SHT85](docs/sht85_connector.png)
SHT85

Alternatively one can also use latching connectors for the [header](https://uk.rs-online.com/web/p/pcb-headers/7455236) on the PCB side and the [socket](https://uk.rs-online.com/web/p/wire-housings-plugs/7973283) on the cable side with suitable [pins](https://uk.rs-online.com/web/p/crimp-contacts/7198753).

It is recommended to use ribbon cables for the small pitch and for less entanglement.

## References
- Original Environmental Monitoring [git repo](https://gitlab.cern.ch/PH-ADE-ID_Pixel_RnD/Temperature_Control) for Arduino firmware and readout scripts
- MoTH v1 [git repo](https://github.com/sdungs/moth)
- [labRemote](https://gitlab.cern.ch/berkeleylab/labRemote)
